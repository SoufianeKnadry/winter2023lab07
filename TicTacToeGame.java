import java.util.Scanner;

public class TicTacToeGame {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome to TIC-TAC-TOE");
		System.out.println("Player 1 starts with \"X\"");
		

		// Initializing all data
		Board table = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;

		// Start

		while (!gameOver) {
			
			System.out.println("---------");
			System.out.println("  0 1 2");
			System.out.println(table);
			System.out.println("---------");

			if (player == 1) {
				playerToken = Square.X;
			} else {
				playerToken = Square.O;
			}

			System.out.println("Player " +player+ " enter a row");
			int row = scan.nextInt();
			System.out.println("Player " +player+" enter a column");
			int column = scan.nextInt();

			while (!table.placeToken(row, column, playerToken)) {
				System.out.println("---------");
				System.out.println("  0 1 2");
				System.out.println(table);
				System.out.println("---------");
				System.out.println("Player " +player+" Re enter row");
				row = scan.nextInt();
				System.out.println("Player " +player+" Re enter column");
				column = scan.nextInt();
			}
			// Check winner/tie
			if (table.checkIfWinning(playerToken)) {
				System.out.println("---------");
				System.out.println(table);
				System.out.println("---------");
				System.out.println("Good Job player " + player + " you won the Tic-Tac-Toe match!");
				gameOver=true;
			}
			else if (table.checkIfFull()) {
				System.out.println("Its a tie!");
				gameOver = true;
			}
			 else {
				if (player == 1) {
					player++;
				} else {
					player = 1;
				}
			}
			
		}

	}
}
