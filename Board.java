public class Board {
	private Square[][] tictactoeBoard;

	public Board() {
		this.tictactoeBoard = new Square[3][3];
		for (int i = 0; i < this.tictactoeBoard.length; i++) {
			for (int y = 0; y < this.tictactoeBoard[i].length; y++) {
				this.tictactoeBoard[i][y] = Square.BLANK;
			}
		}

	}

	public String toString() {

		String table = "";
		for (int i = 0; i < this.tictactoeBoard.length; i++) {
			
			table+=i+" "; 
			for (int y = 0; y < this.tictactoeBoard[i].length; y++) {
	
				table += this.tictactoeBoard[i][y] + " ";
			}

			table += "\n";
		}
		return table;
	}

	public boolean placeToken(int row, int col, Square playerToken) {

		if (row < 0 || row > tictactoeBoard.length-1 || col < 0 || col > tictactoeBoard.length-1) {
			return false;
		} else if (this.tictactoeBoard[row][col] == Square.BLANK) {
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		} else {
			return false;
		}
	}

	public boolean checkIfFull() {
		boolean check = true;
		for (Square[] row : this.tictactoeBoard) {
			for (Square column : row) {
				if (column == Square.BLANK) {
					check = false;
				}
			}
		}
		return check;
	}

	private boolean checkIfWinningHorizontal(Square playerToken) {
		for (int i = 0; i < this.tictactoeBoard.length; i++) {
			int tester = 0;
			for (int y = 0; y < this.tictactoeBoard[i].length; y++) {

				if (this.tictactoeBoard[i][y] == playerToken) {
					tester++;
				}
				if (tester == 3) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkIfWinningVertical(Square playerToken) {

	// same as horizontal just switch y and i in the nested loop
		for (int i = 0; i < this.tictactoeBoard.length; i++) {
			int tester = 0;
			for (int y = 0; y < this.tictactoeBoard[i].length; y++) {

				if (this.tictactoeBoard[y][i] == playerToken) {
					tester++;
				}
				if (tester == 3) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkIfWinningDiagonal(Square playerToken) {

		if (this.tictactoeBoard[0][0] == playerToken && this.tictactoeBoard[1][1] == playerToken
				&& this.tictactoeBoard[2][2] == playerToken) {
			return true;
		}
		if (this.tictactoeBoard[0][2] == playerToken && this.tictactoeBoard[1][1] == playerToken
				&& this.tictactoeBoard[2][0] == playerToken) {
			return true;
		}
		return false;
	}

	public boolean checkIfWinning(Square playerToken) {

		if (checkIfWinningDiagonal(playerToken) || checkIfWinningHorizontal(playerToken)
				|| checkIfWinningVertical(playerToken)) {
			return true;
		} else {
			return false;
		}
	}
}